package com.diorsunion.myassets.dal.repository.test;

import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.myassets.dal.entity.SharesBalance;
import com.diorsunion.myassets.dal.repository.SharesBalanceRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wangnima on 2015/6/4.
 */
public class SharesBalanceRepositoryTest extends RepositoryBaseTest {

    @Autowired
    SharesBalanceRepository repository;

    @Test
    @DataSets({
            @DataSet(entityClass = SharesBalance.class,number=1)
    })
    public void testGet() {
        SharesBalance entity = repository.get(1);
        assert (entity!=null);
    }

    @Test
    @DataSets({
            @DataSet(entityClass = SharesBalance.class,number=4,
                    custom = "thedate={2014-02-02 00:00:00,2014-02-05 00:00:00,2014-02-08 00:00:00,2014-03-03 00:00:00}")
    })
    public void testGetMaxDate() throws ParseException {
        Date date = repository.getMaxDate(new SimpleDateFormat("yyyy-MM-dd").parse("2014-02-07"));
        assert (date.equals(new SimpleDateFormat("yyyy-MM-dd").parse("2014-02-05")));
        date = repository.getMaxDate(new SimpleDateFormat("yyyy-MM-dd").parse("2014-03-03"));
        assert (date.equals(new SimpleDateFormat("yyyy-MM-dd").parse("2014-03-03")));
    }
}
