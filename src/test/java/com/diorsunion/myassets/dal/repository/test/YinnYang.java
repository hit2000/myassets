package com.diorsunion.myassets.dal.repository.test;

import com.diorsunion.dbtest.spring.DBTestClassRunner;
import com.diorsunion.myassets.dal.entity.Shares;
import com.diorsunion.myassets.dal.repository.SharesPriceRepository;
import com.diorsunion.myassets.dal.repository.SharesRepository;
import com.google.common.collect.Lists;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/7/17.
 */
@RunWith(DBTestClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:springbeans-ds-real-test.xml",
        "classpath:springbeans-mybatis.xml"
}) //还是用配置文件方便些
public class YinnYang {

    @Autowired
    SharesPriceRepository sharesPriceRepository;

    @Autowired
    SharesRepository sharesRepository;

    static final List<Integer> sharesIds = Lists.newArrayList(6,7);
    public static final SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
    public static Date begin;
    public static Date end ;

    static {
        try {
            begin = s.parse("2015-01-02");
            end = s.parse("2015-01-02");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void test(){
        Shares s1 = sharesRepository.get(6);
        Shares s2 = sharesRepository.get(6);
//        List<> sharesPriceRepository.findSharePrice(s1,begin,end,"1day");
    }
}
