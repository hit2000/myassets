package com.diorsunion.myassets.dal.repository.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.diorsunion.dbtest.spring.DBTestClassRunner;
import com.diorsunion.myassets.common.CalendarUtils;
import com.diorsunion.myassets.dal.entity.Shares;
import com.diorsunion.myassets.dal.entity.SharesPrice;
import com.diorsunion.myassets.dal.repository.SharesPriceRepository;
import com.diorsunion.myassets.dal.repository.SharesRepository;
import com.google.common.collect.Lists;
import org.apache.http.HttpEntity;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by wangnima on 2015/6/4.
 */
@RunWith(DBTestClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:springbeans-ds-real-test.xml",
        "classpath:springbeans-mybatis.xml"
}) //还是用配置文件方便些
public class SharesPriceDataInitRepositoryTest {

    @Autowired
    SharesPriceRepository sharesPriceRepository;

    @Autowired
    SharesRepository sharesRepository;

    static final URIBuilder uriBuilder = new URIBuilder()
            .setScheme("http")
            .setHost("xueqiu.com")
            .setPath("/stock/forchartk/stocklist.json")
            .setParameter("type", "normal")
            .setParameter("end", String.valueOf(new Date().getTime()))
            .setParameter("_", String.valueOf(System.currentTimeMillis()));
    static final List<String> params = Lists.newArrayList("1day", "1week", "1month");
    static final List<Integer> sharesIds = Lists.newArrayList(1,2,3,4,5,6,7);
    static final List<Shares> sharesList = Lists.newArrayList();
    @Test
    public void testInit() throws Exception {
        sharesIds.forEach(id -> {
            sharesList.add(sharesRepository.get(id));
        });
        sharesList.forEach( shares -> {
            params.forEach( period -> {
                CloseableHttpResponse response1 = null;
                try {
                    uriBuilder.setParameter("period", period).setParameter("symbol", shares.code).build();
                    Date begin = new Date(date20000101.getTime());
                    if(shares.syncEndDate!=null){
                        long endtime = shares.syncEndDate.getTime();
                        long yestime = CalendarUtils.getYestoday().getTime();
                        if(endtime>=yestime){
                            return;
                        }
                        begin.setTime(endtime+86400000);
                    }
                    uriBuilder.setParameter("begin",String.valueOf(begin.getTime()));
                    List<SharesPrice> delete_sharesPriceList = sharesPriceRepository.findSharePrice(shares,begin,null,period);
                    delete_sharesPriceList.forEach( x ->{
                        sharesPriceRepository.delete(x.id,period);
                    });
                    HttpGet httpget = new HttpGet(uriBuilder.build());
                    System.out.println(httpget.getURI());
                    System.out.println("Executing request " + httpget.getRequestLine());
                    httpget.addHeader("Cookie", "s=11c14bel9l; xq_a_token=8e8d27e957be3083f5314a149a74888db7abd47e; xq_r_token=6dfb515a83ead06afa0b2f86535470920f30b315; Hm_lvt_1db88642e346389874251b5a1eded6e3=1436844289,1436933629,1436977162,1437026434; Hm_lpvt_1db88642e346389874251b5a1eded6e3=1437029590; __utma=1.1514965829.1429517117.1437026434.1437029550.14; __utmb=1.3.10.1437029550; __utmc=1; __utmz=1.1436844282.6.5.utmcsr=baidu|utmccn=(organic)|utmcmd=organic");
                    httpget.addHeader("Host", "xueqiu.com");
                    httpget.addHeader("RA-Sid", "3CB01D6F-20140709-112055-4c09cf-c1239f");
                    CookieStore cookieStore = new BasicCookieStore();
                    BasicClientCookie cookie = new BasicClientCookie("Host", "xueqiu.com");
                    cookie.setDomain("xueqiu.com");
                    cookie.setPath("/");
                    cookieStore.addCookie(cookie);
                    CloseableHttpClient httpclient = HttpClients.custom()
                            .setDefaultCookieStore(cookieStore)
                            .build();
                    response1 = httpclient.execute(httpget);
                    HttpEntity entity = response1.getEntity();
                    String s = EntityUtils.toString(entity);
                    JSONObject obj = JSON.parseObject(s);
                    JSONArray chartlist = (JSONArray) obj.get("chartlist");
                    SharesPrice sharesPrice = new SharesPrice();
                    for(int i=0;i<chartlist.size();i++){
                        JSONObject jsonObject = chartlist.getJSONObject(i);
                        JSONObject preObject = null;
                        JSONObject nextObject = null;
                        Date thedate = dateFormat.parse(jsonObject.get("time").toString());
                        if(i==chartlist.size()-1){
                            shares.syncEndDate = thedate;
                        }
                        if(i>0){
                            preObject = chartlist.getJSONObject(i-1);
                        }
                        if(i<chartlist.size()-1){
                            nextObject = chartlist.getJSONObject(i+1);
                        }
                        sharesPrice.chg = Double.parseDouble(jsonObject.get("chg").toString());
                        sharesPrice.close = Double.parseDouble(jsonObject.get("close").toString());
                        sharesPrice.dea = Double.parseDouble(jsonObject.get("dea").toString());
                        sharesPrice.dif = Double.parseDouble(jsonObject.get("dif").toString());
                        sharesPrice.high = Double.parseDouble(jsonObject.get("high").toString());
                        sharesPrice.low = Double.parseDouble(jsonObject.get("low").toString());
                        sharesPrice.ma10 = Double.parseDouble(jsonObject.get("ma10").toString());
                        sharesPrice.ma20 = Double.parseDouble(jsonObject.get("ma20").toString());
                        sharesPrice.ma30 = Double.parseDouble(jsonObject.get("ma30").toString());
                        sharesPrice.ma5 = Double.parseDouble(jsonObject.get("ma5").toString());
                        sharesPrice.macd = Double.parseDouble(jsonObject.get("macd").toString());
                        sharesPrice.open = Double.parseDouble(jsonObject.get("open").toString());
                        sharesPrice.percent = Double.parseDouble(jsonObject.get("chg").toString());
                        sharesPrice.volume = new BigDecimal(jsonObject.get("volume").toString()).longValue();
                        sharesPrice.shares = shares;
                        sharesPrice.turnrate = Double.parseDouble(jsonObject.get("turnrate").toString());
                        sharesPrice.thedate = dateFormat.parse(jsonObject.get("time").toString());
                        sharesPrice.preDate = (i==0&&begin.getTime()!=date20000101.getTime())?begin:preObject!=null?dateFormat.parse(preObject.get("time").toString()):null;
                        sharesPrice.nextDate = nextObject!=null?dateFormat.parse(nextObject.get("time").toString()):null;
                        sharesPriceRepository.insert(sharesPrice, period);
                    }
                    sharesRepository.update(shares);
                    EntityUtils.consume(entity);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(response1!=null){
                        try {
                            response1.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        });
    }

    public static final DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
    public static final DateFormat dateFormat_cn = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
    public static final Date date20000101 = new Date(946656000000L);//2000-01-01 00:00:00 000


}
