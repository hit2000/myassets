package com.diorsunion.myassets.dal.repository.test;

import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.myassets.dal.entity.Shares;
import com.diorsunion.myassets.dal.repository.SharesRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by wangnima on 2015/6/4.
 */
public class SharesRepositoryTest extends RepositoryBaseTest {

    @Autowired
    SharesRepository repository;

    @Test
    @DataSets({
            @DataSet(entityClass = Shares.class,number=1)
    })
    public void testGet() {
        Shares entity = repository.get(1);
        assert (entity!=null);
    }
}
