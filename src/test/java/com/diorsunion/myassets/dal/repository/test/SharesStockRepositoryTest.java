package com.diorsunion.myassets.dal.repository.test;

import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.myassets.dal.entity.Shares;
import com.diorsunion.myassets.dal.entity.SharesStock;
import com.diorsunion.myassets.dal.repository.SharesStockRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by wangnima on 2015/6/4.
 */
public class SharesStockRepositoryTest extends RepositoryBaseTest {

    @Autowired
    SharesStockRepository repository;

    @Test
    @DataSets({
            @DataSet(entityClass = SharesStock.class,number=1)
    })
    public void testGet() {
        SharesStock entity = repository.get(1);
        assert (entity!=null);
    }

    @Test
    @DataSets({
            @DataSet(entityClass = SharesStock.class,number=1,custom = "thedate={2015-05-05 00:00:00};share_id=1"),
            @DataSet(entityClass = Shares.class,number=1,custom = "id=1"),
    })
    public void testFind() throws ParseException {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2015-05-07");
        Date maxDate = repository.getMaxDate(date);
        SharesStock sharesStockQuery = new SharesStock();
        sharesStockQuery.thedate = maxDate;
        List<SharesStock> list = repository.find(sharesStockQuery);
        SharesStock entity = list.get(0);
        assert (entity!=null);
        assert (entity.shares!=null);
        assert (entity.shares.id==1);
    }

}
