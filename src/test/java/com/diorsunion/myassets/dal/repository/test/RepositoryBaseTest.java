package com.diorsunion.myassets.dal.repository.test;

import com.diorsunion.dbtest.spring.DBTestClassRunner;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by wangnima on 2015/6/4.
 */

@RunWith(DBTestClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:springbeans-ds-test.xml",
        "classpath:springbeans-mybatis.xml"
}) //还是用配置文件方便些
public class RepositoryBaseTest {
}
