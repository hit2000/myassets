package com.diorsunion.myassets.dal.repository.test;

import com.alibaba.fastjson.JSON;
import com.diorsunion.dbtest.annotation.DataSet;
import com.diorsunion.dbtest.annotation.DataSets;
import com.diorsunion.myassets.dal.entity.Shares;
import com.diorsunion.myassets.dal.entity.SharesPrice;
import com.diorsunion.myassets.dal.repository.SharesPriceRepository;
import org.apache.http.HttpEntity;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by wangnima on 2015/6/4.
 */
public class SharesPriceRepositoryTest extends RepositoryBaseTest {

    @Autowired
    SharesPriceRepository repository;

    @Test
    @DataSets({
            @DataSet(entityClass = SharesPrice.class,number=1)
    })
    public void testGet() {
        SharesPrice entity = repository.get(1);
        assert (entity!=null);
    }

    @Test
    @DataSets({
            @DataSet(entityClass = SharesPrice.class,number=0)
    })
    public void testInsert() {
        SharesPrice entity = getSharesPrice();
        SharesPrice entity1 = repository.get(1);
        assert (entity1==null);
        repository.insert(entity);
        SharesPrice sharesPrice = repository.get(entity.id);
        assert (entity.chg == sharesPrice.chg);
        assert (entity.close == sharesPrice.close);
        assert (entity.dea == sharesPrice.dea);
        assert (entity.dif == sharesPrice.dif);
        assert (entity.high == sharesPrice.high);
        assert (entity.low == sharesPrice.low);
        assert (entity.chg == sharesPrice.chg);
        assert (entity.chg == sharesPrice.chg);
        assert (entity.chg == sharesPrice.chg);
        assert (entity.chg == sharesPrice.chg);
        assert (entity.chg == sharesPrice.chg);
        assert (entity.chg == sharesPrice.chg);
        assert (entity.chg == sharesPrice.chg);
        assert (entity.chg == sharesPrice.chg);
    }

    public static final SharesPrice getSharesPrice(){
        SharesPrice entity = new SharesPrice();
        Shares shares = new Shares();
        shares.id = 1;
        shares.code = "BABA";
        entity.chg = 0.1;
        entity.close = 0.2;
        entity.dea = 0.3;
        entity.dif = 0.4;
        entity.high = 0.5;
        entity.low = 0.6;
        entity.ma10 = 0.7;
        entity.ma20 = 0.8;
        entity.ma30 = 0.9;
        entity.ma5 = 0.10;
        entity.macd = 0.11;
        entity.open = 0.32;
        entity.percent = 0.13;
        entity.volume = 10;
        entity.shares = shares;
        entity.turnrate = 0.26;
        entity.thedate = new Date();
        return entity;
    }

    public static final DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
}
