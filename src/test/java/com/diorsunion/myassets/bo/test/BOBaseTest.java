package com.diorsunion.myassets.bo.test;

import com.diorsunion.dbtest.spring.DBTestClassRunner;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;

/**
 * Created by Administrator on 2015/4/9.
 */

@RunWith(DBTestClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:springbeans-ds-test.xml",
        "classpath:springbeans-mybatis.xml",
        "classpath:springbeans-bo.xml"
})
@TransactionConfiguration(transactionManager="transactionManager",defaultRollback=false)
public abstract class BOBaseTest {
}
