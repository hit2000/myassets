package com.diorsunion.myassets.dal.entity;

import com.diorsunion.myassets.util.DateUtil;
import com.google.common.collect.Maps;

import java.util.Date;
import java.util.Map;

/**
 * 一天的账户情况
 * Created by Administrator on 2015/7/22.
 */
public class Account {
    public Date date;
    public double balance;//余额
    public Map<Shares,Integer> sharesStocks = Maps.newLinkedHashMap();//股票存储

    public Account(Date date,double balance) {
        this.balance = balance;
        this.date = date;
    }

    /**
     * 买股1
     *
     * @param shares 买什么股
     * @param money 打算花这么多钱来买股，但实际上不一定能买这么多股
     * @param priceType 用什么价格来买股票[开盘价，收盘价，最高价，最低价]
     */
    public void buy(Shares shares, double money,SharesPrice.PriceType priceType) {
        //没钱就不买了
        if(money>balance){
            System.out.println("余额不足");
            return ;
        }
        SharesPrice sharesPrice = shares.getSharesPrice(date);//获取当天的股票价格
        double price = sharesPrice.getPriceByType(priceType);
        int num = (int) (money / price);//能买这么多股
        if(sharesStocks.containsKey(shares)){
            sharesStocks.put(shares,sharesStocks.get(shares)+num);
        }else{
            sharesStocks.put(shares,num);
        }
        double cost = price * num;
        balance -= cost;//买股后增加这么多钱
        System.out.println(DateUtil.getDateFormat(date)+":用"+priceType.name+"买入[" + shares.name + "],单价:"+price+"数量"+num+",总共花费" + String.format("%.2f", cost) + "元,余额:" + String.format("%.2f", balance)  + "元");
    }

    /**
     * 买股2
     *
     * @param shares 买什么股
     * @param num 打算买多少股
     * @param priceType 用什么价格来买股票[开盘价，收盘价，最高价，最低价]
     */
    public void buyBycount(Shares shares, int num,SharesPrice.PriceType priceType) {
        SharesPrice sharesPrice = shares.getSharesPrice(date);//获取当天的股票价格
        double price = sharesPrice.getPriceByType(priceType);
        //没钱就不买了
        if(num*price>balance){
            System.out.println("余额不足");
            return;
        }
        if(sharesStocks.containsKey(shares)){
            sharesStocks.put(shares,sharesStocks.get(shares)+num);
        }else{
            sharesStocks.put(shares,num);
        }
        double cost = price * num;
        balance -= cost;//买股后增加这么多钱
        System.out.println(DateUtil.getDateFormat(date)+":用"+priceType.name+"买入[" + shares.name + "],单价:"+price+"数量"+num+",总共花费" + String.format("%.2f", cost) + "元,余额:" + String.format("%.2f", balance)  + "元");
    }

    /**
     * 卖股
     *
     * @param shares 卖什么股
     * @param money 预计卖股后得到的钱
     * @param priceType 用什么价格来买股票[开盘价，收盘价，最高价，最低价]
     */
    public void sell(Shares shares, double money,SharesPrice.PriceType priceType) {
        SharesPrice sharesPrice = shares.getSharesPrice(date);//获取当天的股票价格
        double price = sharesPrice.getPriceByType(priceType);
        int num = (int) (money / price);//差不多需要卖这么多支股
        if(sharesStocks.containsKey(shares)){
            System.out.println("没有这只股票");
            return;
        }
        if(sharesStocks.get(shares)<num){
            System.out.println("股票数量不够");
            return;
        }
        sharesStocks.put(shares,sharesStocks.get(shares)-num);
        double cost = price * num;
        balance += cost;//卖股后增加这么多钱
        System.out.println(DateUtil.getDateFormat(date)+":用"+priceType.name+"卖出[" + shares.name + "],单价:"+price+"数量"+num+",总共获得" + String.format("%.2f", cost) + "元,余额:" + String.format("%.2f", balance)  + "元");
    }

    /**
     * 卖股
     *
     * @param shares 卖什么股
     * @param num 打算卖多少股
     * @param priceType 用什么价格来买股票[开盘价，收盘价，最高价，最低价]
     */
    public void sellByCount(Shares shares, int num,SharesPrice.PriceType priceType) {
        SharesPrice sharesPrice = shares.getSharesPrice(date);//获取当天的股票价格
        double price = sharesPrice.getPriceByType(priceType);
        if(sharesStocks.containsKey(shares)){
            System.out.println("没有这只股票");
            return;
        }
        sharesStocks.put(shares,sharesStocks.get(shares)-num);
        double cost = price * num;
        balance += cost;//卖股后增加这么多钱
        System.out.println(DateUtil.getDateFormat(date)+":用"+priceType.name+"卖出[" + shares.name + "],单价:"+price+"数量"+num+",总共获得" + String.format("%.2f", cost) + "元,余额:" + String.format("%.2f", balance)  + "元");
    }

    //获取股票价值最高的股票
    public Shares getHighest(SharesPrice.PriceType priceType) {
        double d = 0;
        Shares shares = null;
        for (Map.Entry<Shares,Integer> entry:sharesStocks.entrySet()) {
            Shares s = entry.getKey();
            SharesPrice sharesPrice = s.getSharesPrice(date);
            double price = sharesPrice.getPriceByType(priceType);
            Integer num = entry.getValue();
            double total = price * num;
            if (total > d) {
                d = total;
                shares = s;
            }
        }
        return shares;
    }

    //获取股票价值最低的股票
    public Shares getLowest(SharesPrice.PriceType priceType) {
        double d = Integer.MAX_VALUE;
        Shares shares = null;
        for (Map.Entry<Shares,Integer> entry:sharesStocks.entrySet()) {
            Shares s = entry.getKey();
            SharesPrice sharesPrice = s.getSharesPrice(date);
            double price = sharesPrice.getPriceByType(priceType);
            Integer num = entry.getValue();
            double total = price * num;
            if (total < d) {
                d = total;
                shares = s;
            }
        }
        return shares;
    }

    //获取股票总值
    public double getTotalValue(SharesPrice.PriceType priceType) {
        if(priceType==null){
            priceType = SharesPrice.PriceType.getDefault();
        }
        double total = 0;
        for (Map.Entry<Shares,Integer> entry:sharesStocks.entrySet()) {
            Shares s = entry.getKey();
            SharesPrice sharesPrice = s.getSharesPrice(date);
            double price = sharesPrice.getPriceByType(priceType);
            Integer num = entry.getValue();
            double t = price * num;
            total += t;
        }
        return total + balance;
    }

    @Override
    public String toString() {
        double share_total = getTotalValue(null);
        double total = balance+share_total;
        String s = "账户余额:" + String.format("%.2f", balance)  + ",\t股票总值:" + String.format("%.2f", share_total)+",\t总资产"+String.format("%.2f", total);
        return s;
    }

    public Account initNextDayAccount(Date date) {
        Account account = new Account(date,this.balance);
        account.sharesStocks.putAll(this.sharesStocks);
        return account;
    }
}
