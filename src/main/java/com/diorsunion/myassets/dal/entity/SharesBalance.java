package com.diorsunion.myassets.dal.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by wangnima on 2015/6/4.
 */
public class SharesBalance implements Serializable {
    public int id;
    public int cash;//现金余额
    public int assets;//资产总额
    public Date thedate;//日期
    public int toYestoday;//对比昨天
}
