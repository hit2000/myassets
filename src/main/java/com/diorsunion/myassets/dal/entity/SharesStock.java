package com.diorsunion.myassets.dal.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 股票存储
 * Created by wangnima on 2015/6/4.
 */
public class SharesStock implements Serializable{
    public int id;
    public int num;
    public Shares shares;
    public Date thedate;
}
