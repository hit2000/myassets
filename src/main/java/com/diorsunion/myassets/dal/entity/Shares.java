package com.diorsunion.myassets.dal.entity;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by wangnima on 2015/6/4.
 */
@Entity
public class Shares implements Serializable {
    public Integer id;
    public String code;
    public String name;
    public String organization;
    public String url;
    public Date syncEndDate;//同步截至日期
    public Map<Date,SharesPrice> sharesPriceMap = Maps.newLinkedHashMap();//股票每天的价格

    public SharesPrice getSharesPrice(Date date){
        return sharesPriceMap.get(date);
    }

    public void setSharesPrice(Date date,SharesPrice sharesPrice){
        sharesPriceMap.put(date,sharesPrice);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shares shares = (Shares) o;
        if (code != null ? !code.equals(shares.code) : shares.code != null) return false;
        if (id != null ? !id.equals(shares.id) : shares.id != null) return false;
        if (name != null ? !name.equals(shares.name) : shares.name != null) return false;
        if (organization != null ? !organization.equals(shares.organization) : shares.organization != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (organization != null ? organization.hashCode() : 0);
        return result;
    }
}
