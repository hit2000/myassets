package com.diorsunion.myassets.dal.repository;

import com.diorsunion.myassets.common.BaseRepository;
import com.diorsunion.myassets.dal.entity.Shares;

import javax.annotation.Resource;

/**
 * Created by wangnima on 2015/6/4.
 */
@Resource
public interface SharesRepository extends BaseRepository<Shares,Integer> {
}
