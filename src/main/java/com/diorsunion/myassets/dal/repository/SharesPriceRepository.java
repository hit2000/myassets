package com.diorsunion.myassets.dal.repository;

import com.diorsunion.myassets.common.BaseRepository;
import com.diorsunion.myassets.dal.entity.Shares;
import com.diorsunion.myassets.dal.entity.SharesBalance;
import com.diorsunion.myassets.dal.entity.SharesPrice;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by wangnima on 2015/6/4.
 */
@Resource
public interface SharesPriceRepository extends BaseRepository<SharesPrice,Integer> {
    Date getMaxDate(SharesPrice query);
    void insert(@Param("entity")SharesPrice entity,@Param("period")String period);
    void delete(@Param("id")int id,@Param("period")String period);
    List<SharesPrice> findSharePrice(@Param("shares")Shares shares,@Param("begin")Date begin,@Param("end")Date end,@Param("period")String period);
}
