package com.diorsunion.myassets.dal.repository;

import com.diorsunion.myassets.common.BaseRepository;
import com.diorsunion.myassets.dal.entity.SharesBalance;
import com.diorsunion.myassets.dal.entity.SharesStock;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by wangnima on 2015/6/4.
 */
@Resource
public interface SharesStockRepository extends BaseRepository<SharesStock,Integer> {
    Date getMaxDate(Date date);
}
