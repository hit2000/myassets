package com.diorsunion.myassets.dal.repository;

import com.diorsunion.myassets.common.BaseRepository;
import com.diorsunion.myassets.dal.entity.Shares;
import com.diorsunion.myassets.dal.entity.SharesBalance;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by wangnima on 2015/6/4.
 */
@Resource
public interface SharesBalanceRepository extends BaseRepository<SharesBalance,Integer> {
    Date getMaxDate(Date date);
}
