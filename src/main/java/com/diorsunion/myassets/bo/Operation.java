package com.diorsunion.myassets.bo;

import com.diorsunion.myassets.dal.entity.Account;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * 实现这个接口，进行股票的虚拟买卖操作 ，目的是提高净值
 * Created by Administrator on 2015/7/22.1
 */
public abstract class Operation {
    protected List<Account> accounts = Lists.newArrayList();
    void addAccount(Account account){
        accounts.add(account);
    }
    public abstract void init();
    public abstract void oper();
    public abstract String getDesc();
}
