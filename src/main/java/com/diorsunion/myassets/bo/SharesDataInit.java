package com.diorsunion.myassets.bo;

import com.diorsunion.myassets.dal.entity.Shares;
import com.diorsunion.myassets.dal.entity.SharesPrice;

import java.util.Date;

/**
 * Created by Administrator on 2015/7/23.
 */
public interface SharesDataInit {
    void init(Date begin,Date end,Shares... shares);
}
