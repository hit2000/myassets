package com.diorsunion.myassets.bo;

import com.diorsunion.myassets.bo.operimpl.Oper1;
import com.diorsunion.myassets.bo.sharedatainitimpl.RandomInit;
import com.diorsunion.myassets.dal.entity.Account;
import com.diorsunion.myassets.dal.entity.Shares;
import com.diorsunion.myassets.util.DateUtil;
import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

/**
 * Created by wangnima on 2015/7/16.
 */
public class Win {

    static final double init_money = 500000;
    static List<Operation> opers = new ArrayList<>();
    static {
        opers.add(new Oper1());
//        opers.add(new Oper2());
//        opers.add(new Oper3());
//        opers.add(new Oper4());
    }


    public static void main(String[] args) throws ParseException {
        Date begin = DateUtil.dateFormat.parse("2015-01-01");
        Date end = DateUtil.dateFormat.parse("2015-01-11");
        Shares shares_0 = new Shares();
        shares_0.name="做多";
        Shares shares_1 = new Shares();
        shares_1.name="做空";
        SharesDataInit dataInit = new RandomInit();
        dataInit.init(begin,end,shares_0,shares_1);

        for(Operation oper:opers){
            System.out.println("----------------------------------------------------------------");
            System.out.println("操作方法:"+oper.getDesc());
            List<Account> accounts = Lists.newArrayList();
            Account account = new Account(begin, init_money);
            accounts.add(account);
            for (Date date = DateUtil.addDate(begin,1);date.getTime()<=end.getTime();date = DateUtil.addDate(date,1)) {
                Account nextAccount = account.initNextDayAccount(date);
                oper.addAccount(nextAccount);
                System.out.println(DateUtil.dateFormat.format(date) + "\t操作前:" + account);
                oper.oper();
                System.out.println(DateUtil.dateFormat.format(date) + "\t操作后:" + account);
            }
        }
        System.out.println("*--------------------------------------------------------------*");
//        for(Operation oper:opers){
//            System.out.println(oper.getDesc()+" 操作"+init_money+",随机"+days+"天增减，最后资产结余"+oper.account.getTotalValue());
//        }
    }

}
