package com.diorsunion.myassets.bo;

import com.alibaba.fastjson.JSON;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Administrator on 2015/7/12.
 */
public class StockBO {


    public static void main(String[] args) throws Exception {
//        http://xueqiu.com/stock/forchartk/stocklist.json?symbol=BABA&period=1day&type=normal&begin=1405166656784&end=1436702656784&_=1436702656784
        URI uri = new URIBuilder()
                .setScheme("http")
                .setHost("xueqiu.com")
                .setPath("/stock/forchartk/stocklist.json")
                .setParameter("symbol", "BABA")
                .setParameter("period", "1day")
                .setParameter("type", "normal")
                .setParameter("begin", "1405166656784")
                .setParameter("end", "")
                .setParameter("_", String.valueOf(System.currentTimeMillis()))
                .build();
        HttpGet httpget = new HttpGet(uri);
        System.out.println(httpget.getURI());
        System.out.println("Executing request " + httpget.getRequestLine());
        httpget.addHeader("Cookie","bid=283be63d4784eadf757b39d40681a3e4_iai5svx8; s=4ui12eqfja; xq_a_token=1c670273213657617efb88a9019fd527c0d55a0a; xq_r_token=10c633e5db0ff941bb189eb30904d9d4458711a8; Hm_lvt_1db88642e346389874251b5a1eded6e3=1434378098,1436702463; Hm_lpvt_1db88642e346389874251b5a1eded6e3=1436702484; __utma=1.1671904687.1431009832.1434378098.1436702463.7; __utmb=1.3.10.1436702463; __utmc=1; __utmz=1.1436702463.7.5.utmcsr=baidu|utmccn=(organic)|utmcmd=organic");
        httpget.addHeader("Host","xueqiu.com");
        httpget.addHeader("RA-Sid","3CB01D6F-20140709-112055-4c09cf-c1239f");
        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie = new BasicClientCookie("Host", "xueqiu.com");
        cookie.setDomain("xueqiu.com");
        cookie.setPath("/");
        cookieStore.addCookie(cookie);
        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .build();
        CloseableHttpResponse response1 = httpclient.execute(httpget);
        try {
            System.out.println(response1.getStatusLine());
            HttpEntity entity = response1.getEntity();
            String s = EntityUtils.toString(entity);
            System.out.println(s);
            JSON.parse(s);
            EntityUtils.consume(entity);

        } finally {
            response1.close();
        }
    }
}
