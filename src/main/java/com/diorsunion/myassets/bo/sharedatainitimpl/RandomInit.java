package com.diorsunion.myassets.bo.sharedatainitimpl;

import com.diorsunion.myassets.bo.SharesDataInit;
import com.diorsunion.myassets.dal.entity.Shares;
import com.diorsunion.myassets.dal.entity.SharesPrice;
import com.diorsunion.myassets.util.DateUtil;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;

/**
 * 这个随机算法只适用于 正反指数股
 * Created by Administrator on 2015/7/23.
 */
public class RandomInit implements SharesDataInit {

    static final Random r = new Random(System.currentTimeMillis());

    //获取随机的涨跌
    public static final boolean getRandomUD() {
        return r.nextInt() % 2 == 0;
    }

    //获取随机的涨跌比率
    public static final double getRandomRate() {
        return new BigDecimal(r.nextInt(25) + 5).setScale(2, BigDecimal.ROUND_HALF_UP).divide(new BigDecimal(100)).doubleValue();
    }

    @Override
    public void init(Date begin, Date end, Shares ... shares) {

        SharesPrice sharesPrice_0_begin = new SharesPrice();
        SharesPrice sharesPrice_1_begin = new SharesPrice();
        double b_0 = r.nextInt(70)+30;
        double b_1 = r.nextInt(70)+30;
        sharesPrice_0_begin.open = b_0;
        sharesPrice_0_begin.close = b_0;
        sharesPrice_0_begin.high = b_0;
        sharesPrice_0_begin.low = b_0;
        sharesPrice_1_begin.open = b_1;
        sharesPrice_1_begin.close = b_1;
        sharesPrice_1_begin.high = b_1;
        sharesPrice_1_begin.low = b_1;

        shares[0].setSharesPrice(begin,sharesPrice_0_begin);
        shares[1].setSharesPrice(begin,sharesPrice_1_begin);
        System.out.print(shares[0].name + ":\t" + DateUtil.dateFormat.format(begin) + ":\t" + String.format("%.2f", sharesPrice_0_begin.close)+"\t\t ");
        System.out.println("\t"+shares[1].name+":\t"+DateUtil.dateFormat.format(begin)+":\t"+String.format("%.2f", sharesPrice_1_begin.close)+"\t\t ");
        for(Date date = DateUtil.addDate(begin,1);date.getTime()<=end.getTime();date = DateUtil.addDate(date,1)){
            boolean b = getRandomUD();
            double r = getRandomRate();
            SharesPrice sharesPrice_0 = new SharesPrice();
            SharesPrice sharesPrice_1 = new SharesPrice();
            SharesPrice sharesPrice_yestoday = shares[0].getSharesPrice(DateUtil.addDate(date, -1));
            double close_yestoday = sharesPrice_yestoday.close;
            sharesPrice_0.open = b?close_yestoday*(1+r):close_yestoday*(1-r);
            sharesPrice_0.high = sharesPrice_0.open;
            sharesPrice_0.low = sharesPrice_0.open;
            sharesPrice_0.close = sharesPrice_0.open;
            sharesPrice_1.open = !b?close_yestoday*(1+r):close_yestoday*(1-r);
            sharesPrice_1.high = sharesPrice_0.open;
            sharesPrice_1.low = sharesPrice_0.open;
            sharesPrice_1.close = sharesPrice_0.open;
            shares[0].setSharesPrice(date,sharesPrice_0);
            shares[1].setSharesPrice(date,sharesPrice_1);
            System.out.print(shares[0].name+":\t"+DateUtil.dateFormat.format(date)+":\t"+String.format("%.2f", sharesPrice_0.close)+":\t"+(b?"+":"-")+String.format("%2.0f", r*100d)+"%");
            System.out.println("\t" + shares[1].name + ":\t" + DateUtil.dateFormat.format(date) + ":\t" + String.format("%.2f", sharesPrice_1.close) +":\t"+(!b?"+":"-")+String.format("%2.0f", r*100d)+"%");
        }
    }
}
