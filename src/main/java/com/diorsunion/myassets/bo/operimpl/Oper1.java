package com.diorsunion.myassets.bo.operimpl;

import com.diorsunion.myassets.bo.Operation;
import com.diorsunion.myassets.dal.entity.Account;
import com.diorsunion.myassets.dal.entity.SharesPrice;

/**
 * Created by Administrator on 2015/7/22.
 */
public class Oper1 extends Operation {
    @Override
    public void init() {
        Account account = accounts.get(0);
        account.sharesStocks.forEach((x,y) -> {
            account.buy(x, account.balance / account.sharesStocks.size(), SharesPrice.PriceType.OPEN);
        });
    }

    @Override
    public void oper() {
        System.out.println("-->无操作");
    }

    @Override
    public String getDesc() {
        return "啥也不干";
    }

}
