//package com.diorsunion.myassets.bo.operimpl;
//
//import com.diorsunion.myassets.bo.Operation;
//import com.diorsunion.myassets.dal.entity.Account;
//import com.diorsunion.myassets.dal.entity.SharesPrice;
//
///**
// * Created by Administrator on 2015/7/22.
// */
//public class Oper3 extends Operation {
//    @Override
//    public void init() {
//        Account account = accounts.get(0);
//        account.sharesStocks.forEach((x,y) -> {
//            account.buy(x, account.balance / account.sharesStocks.size()/3, SharesPrice.PriceType.OPEN);
//        });
//    }
//
//    @Override
//    public void oper() {
//        //逢高减仓
//        //逢低补仓
//        for (Stock s : account.stocks) {
//            if (s.rate[current_day] > 0) {
//                int sell_num = (int) (s.num[current_day] * s.rate[current_day]);
//                account.sellByCount(s, sell_num);
//            } else {
//                int buy_num = Math.abs((int) (s.num[current_day] * s.rate[current_day]));
//                account.buyBycount(s, buy_num);
//            }
//        }
//    }
//
//    @Override
//    public String getDesc() {
//        return "逢高减仓,逢低补仓,减仓和补仓的数量的比率就是昨日价格上涨或下降的比率";
//    }
//
//}
