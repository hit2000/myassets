//package com.diorsunion.myassets.bo.operimpl;
//
//import com.diorsunion.myassets.bo.Operation;
//import com.diorsunion.myassets.dal.entity.Account;
//import com.diorsunion.myassets.dal.entity.Shares;
//import com.diorsunion.myassets.dal.entity.SharesPrice;
//
///**
// * Created by Administrator on 2015/7/22.
// */
//public class Oper2 extends Operation {
//    @Override
//    public void init() {
//        Account account = accounts.get(0);
//        account.sharesStocks.forEach((x,y) -> {
//            account.buy(x, account.balance / account.sharesStocks.size(), SharesPrice.PriceType.OPEN);
//        });
//    }
//
//    @Override
//    public void oper() {
//        Shares h_s = account.getHighest();//找出股票价值最高的
//        Stock l_stock = account.getLowest(); //找出股票价值最低的
//        int hvalue = h_stock.getTotalValue();
//        int lvalue = l_stock.getTotalValue();
//        int diff = Math.abs(hvalue - lvalue);//算出两只股票价值的绝对值差
//        account.sell(h_stock, diff / 2);
//        account.buy(l_stock, diff / 2);
//    }
//
//    @Override
//    public String getDesc() {
//        return "每次把涨的那只股 涨的钱的一半卖出去，用同等的钱买跌的那只股";
//    }
//
//}
