//package com.diorsunion.myassets.bo.operimpl;
//
//import com.diorsunion.myassets.bo.Operation;
//import com.diorsunion.myassets.dal.entity.Account;
//import com.diorsunion.myassets.dal.entity.SharesPrice;
//
///**
// * Created by Administrator on 2015/7/22.
// */
//public class Oper4 extends Operation {
//    @Override
//    public void init() {
//        Account account = accounts.get(0);
//        account.sharesStocks.forEach((x,y) -> {
//            account.buy(x, account.balance / account.sharesStocks.size()/2, SharesPrice.PriceType.OPEN);
//        });
//    }
//
//    @Override
//    public void oper() {
//        //逢高减仓
//        //逢低补仓
//        for (Stock s : account.stocks) {
//            if (current_day <= 1) {
//                break;
//            }
//            if (s.rate[current_day] > 0 && s.rate[current_day - 1] < 0) {
//                int buy_num = Math.abs((int) (s.num[current_day] * s.rate[current_day]));
//                account.buyBycount(s, buy_num);
//            }
//            if (s.rate[current_day] < 0 && s.rate[current_day - 1] > 0) {
//                int sell_num = Math.abs((int) (s.num[current_day] * s.rate[current_day]));
//                account.sellByCount(s, sell_num);
//            }
//        }
//    }
//
//    @Override
//    public String getDesc() {
//        return "连续走高不动，连续走高后一直到降低就减仓；连续走低也不动，连续走低直到走高后 就加仓";
//    }
//
//}
