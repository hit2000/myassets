package com.diorsunion.myassets.common;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by 王尼玛 on 2015/4/8.
 */
public interface BaseRepository<E , ID extends Serializable> {
    void insert(E entity);

    void delete(E id);

    void update(E entity);

    E get(ID id);

    List<E> find(E query);

    List<E> findByIds(List<ID> ids);

    Set<E> findToSet(E query);

    Set<E> findToSetByIds(List<ID> ids);

    int count(E query);
}
